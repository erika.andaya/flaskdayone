from bson.objectid import ObjectId

class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def createPost(self, post, username):
        self.conn.insert({'post':post, 'username': username, 'emotion': 'none'})

    def removePost(self, _id):
    	self.conn.remove({'_id': ObjectId(_id)})

    def updatePost(self, _id, emotion, username, post):
        self.conn.update({'_id' : ObjectId(_id) },{'$set': {'emotion': emotion}} );

        




