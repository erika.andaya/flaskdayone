from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session


mod = Blueprint('posts', __name__)

@mod.route('/')
def post_list():
    posts = g.postsdb.getPosts(session['username'])
    return render_template('posts/post.html', posts=posts, date='October 20 2015')

@mod.route('/', methods=['POST'])
def update_post():
	action = request.form['action']
	if action == 'add':
		new_post = request.form['new_post']
		g.postsdb.createPost(new_post, session['username'])
		flash('New post created!', 'create_post_success')
	elif action == 'remove':
		post_id = str(request.form['id'])
		g.postsdb.removePost(post_id)
		flash('Post Deleted!', 'create_post_success')
	else:
		post = request.form['post']
		post_id = str(request.form['id'])
		g.postsdb.updatePost(post_id, action, session['username'], post)
		flash('Emotion Added!', 'create_post_success')


	return redirect(url_for('.post_list'))

	


	